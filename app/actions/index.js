import axios from "axios";

// Action Variables
export const GET_EVENTS = "GET_EVENTS";

// Actions Functions - Make HTTP requests here
// HTTP response data then gets dispatched to Reducer as an action payload
export const getEvents = () => dispatch => {
  axios
    .get("http://localhost:32805/api/events")
    .then(res =>
      dispatch({
        type: GET_EVENTS,
        payload: res.data
      })
    )
    .catch(err => console.log("error ", err));
};
