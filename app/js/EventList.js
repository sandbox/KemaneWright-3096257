import React, { Component } from "react";
import EventCard from "./EventCard";
import { connect } from "react-redux";
import { getEvents } from "../actions";

class EventList extends Component {

  // componentDidMount is a good place to fire your action call and get the initial state for your component
  componentDidMount() {
    this.props.getEvents();
  }

  // Map over your events and pass each event to the <EventCard /> component with their respective props
  render() {
    const { events } = this.props;
    let eventList = "";
    if (events) {
      eventList = events.map(event => {
        return (
          <EventCard
            key={event.nid}
            id={event.nid}
            title={event.title}
            image={event.url}
            body={event.body}
          />
        );
      });
    }
    return <div>{eventList}</div>;
  }
}

// Map redux store state to component props
const mapStateToProps = state => ({
  events: state.events
});


// If you need to access your Redux store from a component you must use the connect function from react-redux
export default connect(
  mapStateToProps,
  { getEvents }
)(EventList);