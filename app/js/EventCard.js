import React, { Component } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText
} from "mdbreact";

// This component does not need to directly access the redux store so we can skip the connect function
// And we just return a pre styled card element passing in each event's props
export default class EventCard extends Component {
  render() {
    return (
      <MDBCard style={{ width: "22rem" }}>
        <MDBCardImage className="img-fluid" src={this.props.image} waves />
        <MDBCardBody>
          <MDBCardTitle>{this.props.title}</MDBCardTitle>
          <MDBCardText>{this.props.body}</MDBCardText>
        </MDBCardBody>
      </MDBCard>
    );
  }
}
