import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from '../store';
// Import your custom components
import EventList from './EventList';

// This is your root component mainly used for importing other components - little to no work should be done in here
// Render custom components as "HTML" elements below ex. <EventList />
// If you chose not use Redux you can make HTTP requests here 
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <React.Fragment>
          <p>Replace with React Components</p>
          <EventList />
        </React.Fragment>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("ReactBlock"));
