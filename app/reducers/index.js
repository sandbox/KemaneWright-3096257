import { GET_EVENTS } from "../actions";

// Define the initial state of your Redux store
const initialState = {
  events: []
};

// Use a Switch or If/Else statement to handle multiple action types -
// Get, Add, Delete, etc.
// Set state to action payload (HTTP response data)
export default function(state = initialState, action) {
  switch (action.type) {
    case GET_EVENTS:
      return {
        ...state,
        events: action.payload
      };
    default:
      return state;
  }
}
